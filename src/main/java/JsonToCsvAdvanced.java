import com.github.opendevl.JFlat;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class JsonToCsvAdvanced {
    public static void main(String args []) {
        try {
            String str = new String(Files.readAllBytes(Paths.get("cc-dlq-data.json")));
            JFlat flatMe = new JFlat(str);
            //get the 2D representation of JSON document
            List<Object[]> json2csv = flatMe.json2Sheet().getJsonAsSheet();
            //write the 2D representation in csv format
            flatMe.write2csv("cc-dlq-data.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
